package main

import (
	"encoding/json"
	//"encoding/json"
	"flag"
	"github.com/alecthomas/repr"
	"gitlab.com/coalang/go-compiler/compiler"
	"gitlab.com/coalang/go-compiler/pkg/base"
	"time"

	//"gitlab.com/coalang/go-compiler/compiler"
	//"io/ioutil"
	"os"
	"runtime/debug"
	"runtime/trace"
	"sync"
	//"gitlab.com/coalang/go-compiler/compiler"
	"gitlab.com/coalang/go-compiler/parser"
	//"gitlab.com/coalang/go-compiler/pkg/base"
	//"gitlab.com/coalang/go-compiler/pkg/compress"
	"gitlab.com/coalang/go-compiler/ui"
)

func recoverAndExit() {
	if r := recover(); r != nil {
		ui.Debugf("Recovered: %v", r)
		ui.Debugf(
			"Stack Trace:\n%s",
			string(debug.Stack()),
		)
		ui.Forcef("Exiting with code 1.")
		os.Exit(1)
	}
}

func main() {
	start := time.Now()
	ui.Forcef("Coa Go Compiler v0 (experimental)")

	defer recoverAndExit()

	var compiledIn, ranIn time.Duration
	var compileDone sync.WaitGroup

	compileDone.Add(3)

	astDone, runDone := make(chan struct{}), make(chan struct{})

	var doTrace bool
	var logLevelRaw, inSrc, outSrc, fmtSrc string

	flag.BoolVar(&doTrace, "trace", false, "Dump Go runtime trace.")
	flag.StringVar(&logLevelRaw, "log", "info", "Input inFile path.")
	flag.StringVar(&inSrc, "in", "./in.coa", "Input inFile path.")
	flag.StringVar(&outSrc, "out", "./out.json", "Output inFile path.")
	flag.StringVar(&fmtSrc, "fmt", "./fmt.coa", "Formatted inFile path.")
	flag.Parse()

	//if flag.NArg() == 0 {
	//	flag.Usage()
	//	ui.Panicf("Error while parsing flags: ")
	//} else {
	//	flag.Args()
	//}

	ui.Level = ui.GetLevel(logLevelRaw)

	ui.Forcef("Log level is \"%s\".", ui.Level)

	if doTrace {
		traceFile, err := os.Create(".coa-go-compiler.trace")
		if err != nil {
			ui.Panicf("Error while opening/creating traceFile to save trace in: %v", err)
		}

		err = trace.Start(traceFile)
		if err != nil {
			ui.Panicf("Error while starting trace: %v", err)
		}

		defer trace.Stop()
	}

	inFile, err := os.Open(inSrc)
	if err != nil {
		ui.Panicf("Error while opening inFile: %v", err)
	}

	defer inFile.Close()
	parserAST, err := parser.Parse(inSrc, inFile)
	if err != nil {
		ui.Panicf("Error while parsing: %v", err)
	}

	var block compiler.Obj = &compiler.Block{}
	AST := block.From(parserAST)

	var JSON []byte

	go func() {
		defer recoverAndExit()
		var err error
		JSON, err = json.Marshal(AST)
		if err != nil {
			ui.Errorf("Error while marshalling AST to JSON: %v", err)
		}

		astDone <- struct{}{}

		err = ioutil.WriteFile(outSrc, JSON, 0644)

		if err != nil {
			ui.Errorf("Error while saving AST as JSON: %v", err)
		}
		compileDone.Done()
	}()
	go func() {
		defer recoverAndExit()
		<-astDone

		gzipSrc, err := compress.Compress(JSON)
		err = ioutil.WriteFile(outSrc+".gzip", gzipSrc.Bytes(), 0644)

		if err != nil {
			ui.Errorf("Error while saving AST as gzipped JSON: %v", err)
		}

		compileDone.Done()
	}()
	go func() {
		defer recoverAndExit()
		coaSrc := AST.CodeBare()
		err = ioutil.WriteFile(fmtSrc, []byte(coaSrc), 0644)

		if err != nil {
			ui.Errorf("Error while saving formatted source as Coa: %v", err)
		}

		compileDone.Done()
	}()
	go func() {
		defer recoverAndExit()
		compileDone.Wait()

		compiledIn = time.Since(start)
	}()

	go func() {
		defer recoverAndExit()
		ctx := compiler.Context{
			Ctx: nil,
			Mem: base.Base,
		}
		_, err = AST.Eval(&ctx)

		if err != nil {
			ranIn = time.Since(start)
			ui.Panicf("Error while running: %v", err)
		}

		ranIn = time.Since(start)

		runDone <- struct{}{}
	}()

	compileDone.Wait()
	ui.Okf("Compiled and saved in %v.", compiledIn)

	_ = <-runDone

	ui.Okf("Ran in %v.", ranIn)
	repr.Println(parserAST)
}
