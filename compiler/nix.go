package compiler

import (
	"gitlab.com/coalang/go-compiler/parser"
	"gitlab.com/coalang/go-compiler/types"
)

type Nix struct {
	Ctx *types.SrcContext `json:"c"`
}

func (n *Nix) From(thing parser.Thing) Obj {
	return NewNix()
}

func (n *Nix) SrcCtx() *types.SrcContext {
	return n.Ctx
}

func (n *Nix) Code() string {
	return "..nix"
}

func (n *Nix) Eval(ctx *Context) (Obj, error) {
	return n, nil
}

func (n *Nix) Child(s string) Obj {
	return nil
}

func (n *Nix) String() string {
	return "..nix" // copy function instead of calling n.Code to (possibly) shorten stack / optimize
}

func NewNix(ctx *types.SrcContext) Obj {
	tmp := Nix{
		Ctx: ctx,
	}
	return &tmp
}
