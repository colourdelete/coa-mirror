package compiler

import (
	"github.com/alecthomas/repr"
	"gitlab.com/coalang/go-compiler/parser"
	"gitlab.com/coalang/go-compiler/signals"
	"gitlab.com/coalang/go-compiler/types"
	"gitlab.com/coalang/go-compiler/ui"
)

type Block struct {
	Ctx *types.SrcContext `json:"c"`

	Sign *Map  `json:"s"`
	Objs []Obj `json:"o"`
}

func (b *Block) From(thing parser.Thing) Obj {
	switch thing.(type) {
	case parser.RawBlock:
		rb := thing.(parser.RawBlock)
		b.Objs = rb.Objs.
	}
}

func NewBlock(ctx *types.SrcContext, sign *Map, objs []Obj) *Block {
	tmp := Block{
		Ctx:  ctx,
		Sign: sign,
		Objs: objs,
	}

	return &tmp
}

func (b *Block) Eval(ctx *Context) (Obj, error) {
	var err error
	var re Obj

	for _, obj := range b.Objs {

		if obj == nil {
			continue
		}

		re, err = obj.Eval(ctx)
		ui.Debugf("mem: %s", repr.String(ctx, repr.Indent("  ")))

		if re == nil {
			ui.Debugf("<nil>")
		} else {
			ui.Debugf("%s", re)
		}

		switch err.(type) {
		case signals.Return: // TODO: implement ..re()
			return re, nil
		default:
			if err != nil {
				return nil, err
			}
		}
	}

	return nil, nil
}

// Child returns the child of the Block in dot format.
// Child("a.b.c") equals <block>.a.b.c.
func (b *Block) Child(string) Obj {
	return nil // TODO: add special functions like ..to, etc
}

func (b *Block) CodeBare() string {
	re := ""
	for _, obj := range b.Objs {
		re += obj.Code()
		if len(b.Objs) > 1 {
			re += "\n"
		}
	}

	return re
}

func (b *Block) Code() string {
	re := "[\n"
	for _, obj := range b.Objs {
		re += IndentStr(obj.Code(), "  ")
		if len(b.Objs) > 1 {
			re += "\n"
		}
	}

	re += "\n]"

	return re
}

func (b *Block) SrcCtx() *types.SrcContext {
	return b.Ctx
}

func (b *Block) String() string {
	return b.Code()
}
