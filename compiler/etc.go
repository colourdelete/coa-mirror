package compiler

import "strings"

func IndentStr(str string, indent string) string {
	return strings.ReplaceAll(
		str,
		"\n",
		"\n"+indent,
	)
}

func UnindentStr(str string, indent string) string {
	return strings.ReplaceAll(
		str,
		"\n"+indent,
		"\n",
	)
}
