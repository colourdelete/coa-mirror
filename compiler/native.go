package compiler

import (
	"fmt"
	"gitlab.com/coalang/go-compiler/types"
)

type NativeFunc func(*types.SrcContext, *Context) (Obj, error)

type Native struct {
	Ctx *types.SrcContext `json:"c"`

	Sign *Map       `json:"s"`
	Func NativeFunc `json:"n"`
}

func NewNative(ctx *types.SrcContext, sign *Map, f NativeFunc) *Native {
	tmp := Native{
		Ctx:  ctx,
		Sign: sign,
		Func: f,
	}
	return &tmp
}

func (n *Native) SrcCtx() *types.SrcContext {
	return n.Ctx
}

func (n *Native) Code() string {
	return fmt.Sprintf("%s[{ native func %s }]", n.Sign, n.Func)
}

func (n *Native) Eval(ctx *Context) (Obj, error) {
	return n.Func(n.SrcCtx(), ctx)
}

func (n *Native) Child(string) Obj {
	return nil
}

func (n *Native) String() string {
	return n.Code()
}
