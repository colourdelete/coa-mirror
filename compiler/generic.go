package compiler

import (
	"fmt"
	"gitlab.com/coalang/go-compiler/signals"
	"gitlab.com/coalang/go-compiler/types"
	"strings"
)

type Generic struct {
	Ctx *types.SrcContext

	Objs map[string]Obj
}

type Genericer interface {
	Generic() *Generic
}

func NewGeneric(ctx *types.SrcContext, objs map[string]Obj) *Generic {
	tmp := Generic{
		Ctx:  ctx,
		Objs: objs,
	}
	return &tmp
}

func (g *Generic) SrcCtx() *types.SrcContext {
	return g.Ctx
}

func (g *Generic) Code() string {
	return fmt.Sprintf("{ generic object %v }", &g)
}

func (g *Generic) Eval(*Context) (Obj, error) {
	return nil, signals.Uncallable{
		Ctx: g.Ctx,
		Ref: "",
	}
}

func (g *Generic) SetChild(sRaw string, obj Obj) {
	if sRaw == "" {
		panic("internal error: Generic.Child(sRaw) - sRaw is none")
	}

	refIns := strings.Split(sRaw, ".") // if len = 0, sRaw must = ""

	if len(refIns) > 1 {
		g.SetChild(strings.Join(refIns[1:], "."), obj)
	} else {
		g.Objs[refIns[0]] = obj
	}
}

func (g *Generic) Child(sRaw string) Obj {
	if sRaw == "" {
		panic("internal error: Generic.Child(sRaw) - sRaw is none")
	}

	refIns := strings.Split(sRaw, ".") // if len = 0, sRaw must = ""

	//var obj Obj
	//var ok bool
	//var objs = g.Objs
	// TODO: v optimize cutting off first elem
	if len(refIns) > 1 {
		if g.Objs[refIns[0]] != nil {
			return g.Objs[refIns[0]].Child(strings.Join(refIns[1:], "."))
		} else {
			//ui.Panicf("%s", signals.Invalid{
			//	Ctx: g.Ctx,
			//	M:   "g.Objs[refIns[0]] is nil OR implement special methods (eg ..=)",
			//})
			return nil
		}
	} else {
		return g.Objs[refIns[0]]
	}
	//for _, refIn := range refIns {
	//	obj, ok = objs[refIn]
	//
	//	keys := make([]string, 0, len(objs))
	//	for k, _ := range objs { // TODO: rm soon, only for debugging
	//		keys = append(keys, k)
	//	}
	//
	//	if !ok {
	//		return nil
	//	}
	//
	//}
	//
	//return obj
}

func (g *Generic) Merge(g2 Generic) Generic {
	objs := g.Objs
	for k, v := range g2.Objs {
		objs[k] = v // g2 has priority over g
	}
	return Generic{
		Ctx:  g.Ctx, // always takes context of original (g, not g2)
		Objs: objs,
	}
}

func (g *Generic) String() string {
	return g.Code()
}
