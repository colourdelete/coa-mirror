package signals

import (
	"gitlab.com/coalang/go-compiler/types"
)

type Signal interface {
	error
	SrcCtx() *types.SrcContext
	Msg() string
}
