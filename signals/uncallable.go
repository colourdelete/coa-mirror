package signals

import (
	"fmt"
	"gitlab.com/coalang/go-compiler/types"
)

type Uncallable struct {
	Ctx *types.SrcContext

	Ref string
}

func (u Uncallable) SrcCtx() *types.SrcContext {
	return u.Ctx
}

func (u Uncallable) Msg() string {
	return fmt.Sprintf("%s is uncallable", u.Ref)
}

func (u Uncallable) Error() string {
	ctx := ""
	if u.Ctx != nil {
		ctx = u.Ctx.Fmt() + " "
	}
	msg := ""
	if u.Msg() != "" {
		msg = u.Msg()
	}
	return fmt.Sprintf("%s%s", ctx, msg)
}
