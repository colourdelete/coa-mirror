package signals

import (
	"fmt"
	"gitlab.com/coalang/go-compiler/types"
)

type Return struct {
	Ctx *types.SrcContext
}

func (r Return) SrcCtx() *types.SrcContext {
	return r.Ctx
}

func (r Return) Msg() string {
	return fmt.Sprintf("this is a return signal")
}

func (r Return) Error() string {
	ctx := ""
	if r.Ctx != nil {
		ctx = r.Ctx.Fmt() + " "
	}
	msg := ""
	if r.Msg() != "" {
		msg = r.Msg()
	}
	return fmt.Sprintf("%s%s", ctx, msg)
}
