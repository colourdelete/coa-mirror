package signals

import (
	"fmt"
	"gitlab.com/coalang/go-compiler/types"
)

type Unreal struct {
	Ctx *types.SrcContext

	Ref string
}

func (u Unreal) SrcCtx() *types.SrcContext {
	return u.Ctx
}

func (u Unreal) Msg() string {
	return fmt.Sprintf("%s is unreal", u.Ref)
}

func (u Unreal) Error() string {
	ctx := ""
	if u.Ctx != nil {
		ctx = u.Ctx.Fmt() + " "
	}
	msg := ""
	if u.Msg() != "" {
		msg = u.Msg()
	}
	return fmt.Sprintf("%s%s", ctx, msg)
}
