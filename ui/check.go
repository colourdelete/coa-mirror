package ui

func GetLevel(levelName string) level {
	levels := map[string]level{
		"force":   LevelForce,
		"0":       LevelForce,
		"fatal":   LevelFatal,
		"f":       LevelFatal,
		"1":       LevelFatal,
		"error":   LevelError,
		"e":       LevelError,
		"2":       LevelError,
		"warning": LevelWarning,
		"w":       LevelWarning,
		"3":       LevelWarning,
		"ok":      LevelOk,
		"o":       LevelOk,
		"4":       LevelOk,
		"info":    LevelInfo,
		"i":       LevelInfo,
		"5":       LevelInfo,
		"debug":   LevelDebug,
		"d":       LevelDebug,
		"6":       LevelDebug,
	}
	if level, ok := levels[levelName]; ok {
		return level
	} else {
		return 10
	}
}
