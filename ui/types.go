package ui

import "strconv"

type level uint8

func (l level) String() string {
	switch l {
	case LevelForce:
		return "force"
	case LevelFatal:
		return "fatal"
	case LevelError:
		return "error"
	case LevelWarning:
		return "warning"
	case LevelOk:
		return "ok"
	case LevelInfo:
		return "info"
	case LevelDebug:
		return "debug"
	default:
		return strconv.FormatUint(uint64(l), 10)
	}
}

type Printer interface {
	Log(level, string)
	Logf(level, string, ...interface{})
	Logln(level, string)
	Slog(level, string)
	Slogf(level, string, ...interface{})
	Slogln(level, string)
	Flog(level, string)
	Flogf(level, string, ...interface{})
	Flogln(level, string)
}
