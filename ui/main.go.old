package ui

import "io"

var (
	IconNil   = " "
	IconInfo  = "i"
	IconWarn  = "!"
	IconError = "x"
	IconDebug = "d"
	IconOk    = "/"
)

type Printer interface {
	Fprint(io.Writer, Status)
	Print(Status)
}

type Status struct {
	Progress Fraction
	Icon     string
	Message  string
}

// Fraction represents a fraction.
// N represents numerator, and D represents denominator.
// For example, 1/2 would be `Fraction { 1 2 }`.
type Fraction struct {
	N, D int
}
