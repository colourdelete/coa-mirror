package simple

import (
	"github.com/alecthomas/participle/v2/lexer"
)

type Program struct {
	Pos    lexer.Position
	EndPos lexer.Position
	Tokens []lexer.Token

	Uses []Use `parser:"@@*" json:"u"`
}

type Use struct {
	Pos    lexer.Position
	EndPos lexer.Position
	Tokens []lexer.Token

	Stat  string `parser:"@Use" json:"-"`
	Ident Str    `parser:"@@" json:"s"`
}

type Str struct {
	Pos    lexer.Position
	EndPos lexer.Position
	Tokens []lexer.Token

	In string `parser:"@StrIn"`
}
