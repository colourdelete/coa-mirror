package parser

import (
	"fmt"
	"github.com/alecthomas/participle/v2"
	"strings"
)

type ParseError struct {
	wrapped participle.Error
	lines   []string
	src     string
}

func NewParseErrorFromError(err participle.Error, src string) ParseError {
	return ParseError{
		wrapped: err,
		lines:   strings.Split(src, "\n"),
		src:     src,
	}
}

func repStr(src, str string, idx int) string {
	src += " "
	return src[:idx] + str + src[idx+1:]
}

func (e ParseError) getLines(a, b int) string {
	re := ""
	maxLineIdx := len(e.lines) - 1
	for lineIdx := a; lineIdx <= b; lineIdx++ {
		if lineIdx < 0 || lineIdx > maxLineIdx {
			continue
		}
		line := e.lines[lineIdx]
		re += e.line(lineIdx+1, 4) + line + "\n"
	}
	return re
}

func (e ParseError) line(lineIdx, digitsLen int) string {
	return fmt.Sprintf(fmt.Sprintf("%%-%vv ", digitsLen), lineIdx)
}

func (e ParseError) Error() string {
	re := e.wrapped.Error()
	re += "\n"
	pos := e.wrapped.Position()
	lineNum := pos.Line - 1
	colNum := pos.Column - 1
	offset := 3

	if len(e.lines) > lineNum {
		//if lineNum-2 >= 0 {
		//	re += e.lines[lineNum-2] + "\n"
		//	if lineNum-1 >= 0 {
		//		re += e.lines[lineNum-1] + "\n"
		//	} else {
		//		re += "(start of file)\n"
		//	}
		//} else {
		//	re += "(start of file)\n\n"
		//}
		//lenDigits := len(strconv.FormatInt(int64(lineNum+offset+1), 10))
		re += e.getLines(lineNum-offset-1, lineNum-1)
		re += strings.Repeat(" ", 5) + strings.Repeat(" ", colNum) + "v\n"
		re += e.line(lineNum+1, 4) + e.lines[lineNum] + "\n"
		re += strings.Repeat(" ", 5) + strings.Repeat(" ", colNum) + "^\n"
		re += e.getLines(lineNum+1, lineNum+offset+1)
		//if lineNum+1 < len(e.lines) {
		//	re += e.lines[lineNum+1] + "\n"
		//	if lineNum+2 < len(e.lines) {
		//		re += e.lines[lineNum+2]
		//	} else {
		//		re += "(end of file)"
		//	}
		//} else {
		//	re += "\n(end of file)"
		//}
	} else {
		re += fmt.Sprintf(
			"source position not found: %v:%v",
			lineNum,
			colNum,
		)
	}
	return re
}
