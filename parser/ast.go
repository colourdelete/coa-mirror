package parser

import (
	"github.com/alecthomas/participle/v2/lexer"
)

type LexerInfo struct {
	Pos    lexer.Position
	EndPos lexer.Position
	Tokens []lexer.Token
}

type Thing interface {
	SrcCtx() LexerInfo
}

type RawBlock struct {
	Thing
	LexerInfo
	Objs *Objs `@@?`
}

type Objs struct {
	Thing
	LexerInfo
	Left Obj      `@@`
	In   []ObjsIn `@@*`
}

type ObjsIn struct {
	Thing
	LexerInfo
	Div   string `@Space`
	Right Obj    `@@`
}

type Obj struct {
	Thing
	LexerInfo
	In       ObjIn    `@@`
	Suffixes []Suffix `@@*`
}

type ObjIn struct {
	Thing
	LexerInfo
	Block *Block `(@@`
	Text  *Text  `|@@`
	Map   *Map   `|@@`
	Ref   *Ref   `|@@)`
}

type Suffix struct {
	Thing
	LexerInfo
	Map    *MapRaw `@@`
	Method *Method `|@@`
}

type Method struct {
	Thing
	LexerInfo
	Dot string `@Dot`
	Ref Ref    `@@`
	Map MapRaw `@@`
}

type Block struct {
	Thing
	LexerInfo
	OBrack string `@OBrack`
	Objs   *Objs  `@@?`
	CBrack string `@CBrack`
}

type Text struct {
	Thing
	LexerInfo
	Text string `@TextIn`
}

type Map struct {
	Thing
	LexerInfo
	OParen string `@OParen`
	Objs   *Objs  `@@?`
	CParen string `@CParen`
	Block  *Block `@@?`
}

type MapRaw struct {
	Thing
	LexerInfo
	OParen string `@OParen`
	Objs   *Objs  `@@?`
	CParen string `@CParen`
}

type Ref struct {
	Thing
	LexerInfo
	Ref string `(@Dot|@RefIn)+`
}
