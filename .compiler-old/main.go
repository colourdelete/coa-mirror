package _compiler_old

import (
	"errors"
	"fmt"

	"gitlab.com/coalang/go-compiler/parser"
)

// Compile is a function that converts RawBlock to GO source code and an error.
func Compile(ast *parser.RawBlock) (string, error) {
	if ast == nil {
		return "", fmt.Errorf("ast is %s", fmt.Sprint(ast))
	}
	objs, err := CompileObjects(ast.Objects)
	if err != nil {
		return "", err
	}
	var src = templateStart + fmt.Sprintf("coa.Block(%s).Run()", objs) + templateEnd
	return src, nil
}

func CompileObjects(objs []*parser.Object) (string, error) {
	var src, tmp string
	var err error
	for _, obj := range objs {
		tmp, err = CompileObject(obj)
		if err != nil {
			return "", err
		}
		src += tmp
		src += ","
	}
	return src, nil
}

func CompileObject(obj *parser.Object) (string, error) {
	if obj.Text != nil {
		return obj.Text.Text, nil
	} else if obj.Block != nil {
		return CompileBlock(obj.Block)
	} else if obj.Map != nil {
		return CompileMap(obj.Map)
	} else if obj.Call != nil {
		return CompileCall(obj.Call)
	} else if obj.Ref != nil {
		return CompileRef(obj.Ref)
	} else {
		return "", errors.New("Object does not contain any data: nil or \"\"")
	}
}

func CompileBlock(block *parser.Block) (string, error) {
	tmp, err := CompileObjects(block.Objects)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("coa.Block(%s)", tmp), nil
}

func CompileMap(map_ *parser.Map) (string, error) {
	tmp, err := CompileObjects(map_.Objects)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("coa.Map(%s)", tmp), nil
}

func CompileCall(call *parser.CallMaybe) (string, error) {
	ref, err := CompileRef(call.Ref)
	if err != nil {
		return "", err
	}
	map_, err := CompileMap(call.Map)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("coa.Eval(%s, %s)", ref, map_), nil
}

func CompileRef(ref *parser.ObjRef) (string, error) {
	return fmt.Sprintf("coa.ObjRef(\"%s\")", ref.Ident), nil
}
