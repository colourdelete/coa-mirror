package compress

import (
	"bytes"
	"compress/gzip"
	"io/ioutil"
)

func Compress(src []byte) (bytes.Buffer, error) {
	buf := bytes.NewBuffer([]byte{})
	gz := gzip.NewWriter(buf)

	_, err := gz.Write(src)
	if err != nil {
		return bytes.Buffer{}, err
	}

	err = gz.Close()
	if err != nil {
		return bytes.Buffer{}, err
	}
	return *buf, nil
}

func Decompress(buf bytes.Buffer) ([]byte, error) {
	gz, err := gzip.NewReader(&buf)
	if err != nil {
		return []byte{}, err
	}

	src, err := ioutil.ReadAll(gz)
	if err != nil {
		return []byte{}, err
	}

	err = gz.Close()
	if err != nil {
		return []byte{}, err
	}

	return src, err
}
